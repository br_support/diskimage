# Popis problému
B&R bohužel nemá žádný nástroj kterým lze inteligentně vytvořit image CFast karty. 
Nástroje 
[Runtime Utility Center](https://www.br-automation.com/cs/soubory-ke-stazeni/software/automation-netpvi/pvi-development-setup/) a 
[Embedded OS Installer](https://www.br-automation.com/cs/soubory-ke-stazeni/software/operating-systems/windows-ce-42/embedded-os-installer/)
vytváří bitové kopie a nepodporují funkci
[TRIM](https://cs.wikipedia.org/wiki/TRIM).
K zálohování CFast je proto nutné použít SW třetích stran. Dále je popsáno zálohování 
pomocí volně stažitelného softwaru
[Mactium Reflect](https://www.macrium.com/reflectfree).

# Macrium Reflect - instalace
1. Jděte na stránky [www.macrium.com/reflectfree](https://www.macrium.com/reflectfree).
2. Klikněte na *Home Use* <br> ![](/pic1.png)
3. Zadejte email <br> ![](/pic2.png)
4. Do mailu vám příjde odkaz na stažení *Macrium Reflect*
5. Klikněte na odkaz a spusťte stažený instalátor <br>![](/pic4.png)
6. Kliněte na *Download* <br>![](/pic5.png)
7. Na tomto dialogu zrušte checkbox <br>![](/pic6.png)

# Macrium Reflect - záloha disku
1. Vložte CFast kartu do čtečky a připojte k PC
2. Po stisknutí *Refresh* objeví nový disk <br>![](/pic7.png)
3. Záloha se provede kliknutíém na "Image this disk" <br>![](/pic8.png)
4. Vyberete kam se má uložit image <br>![](/pic9.png)
5. Proklikejte dialogy až do konce, finální stav vypadá takto <br>![](/pic10.png)

# Macrium Reflect - obnova disku ze zálohy
1. Vyberte záložku *Restore* <br> ![](/pic11.png)
2. Klikněte na *Browse for...* a vyberte *.mrimg* image <br> ![](/pic12.png)
3. Klikněte na *Restore Image* <br> ![](/pic13.png)
4. Klikněte na *Select a disk to restore to* <br> ![](/pic14.png)
5. Vyberte CFast kartu na kterou se image má nahrát <br> ![](/pic15.png)
6. Klikněte na *Next* a *Finish* <br> ![](/pic16.png)